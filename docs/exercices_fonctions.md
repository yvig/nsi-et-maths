
## Exercices de base

### Exercice 1

``` python
        def nb_boites(n : int) -> int:
            """ renvoie le nombre de boîtes de 6 oeufs nécessaires pour ranger n oeufs
            précondition : n >= 0
            """ 
    	# à vous de compléter, l'énoncé est dans la docstring
```
