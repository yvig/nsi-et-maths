
Il arrive très souvent en programmation qu’une suite d'instructions un peu complexe revienne régulièrement, qu’un
traitement de données nécessite plusieurs fois le même algorithme... les fonctions sont là pour nous aider dans ces
tâches !

## Définition d'une fonction

Une fonction possède un nom pour pouvoir être appelée et il est possible de lui communiquer des **paramètres** (ou **arguments**).

En python la syntaxe de définition d’une fonction est la suivante:

!!! info "Syntaxe"
    === "Définition avec `return`"

        ``` python
        def nom_de_ma_fonction(paramètres séparés par des virgules):
            instruction 1
            instruction 2
            ...
            return objet_renvoyé_par_ma_fonction
        ```
        Attention à l'indentation !

    === "Définition sans `return`"

        ``` python
        def nom_de_ma_fonction(paramètres séparés par des virgules):
            instruction 1
            instruction 2
            ...
        ```
        Attention à l'indentation !  
        
    === "Appel"
        Pour appeler la fonction, il suffit de taper son nom avec les arguments entre parenthèses : 

        ``` python
        nom_de_ma_fonction(arg1, arg2,...)
        ```


??? tip "Comment nommer une fonction ?"

    En python, la convention PEP8 préconise de nommer les fonctions et variables avec des lettres minuscules et des tirets bas `_` pour remplacer les espaces.



## Documenter une fonction 

Il peut paraître inutile de documenter ses fonctions, c'est-à-dire d'écrire des commentaires pour préciser son utilité.

C'est pourtant très important d'en prendre l'habitude, surtout si une fonction est réutilisée dans d'autres programmes ou par d'autres personnes. Or le travail en groupe est une pratique fréquente dans les études d'informatique, mais aussi dans les métiers de cette branche. 

### Annotations de type
Pour indiquer de quels types sont les paramètres de la fonction et de quel type est la variable renvoyée, on les ajoute à la première ligne de la fonction comme dans l'exemple suivant :

``` python
def est_pair(n : int) -> bool :
    return n % 2 == 0
```

Il est indiqué ici que l'argument attendu est un entier (`n : int`) et que la fonction renvoie un booléen (` -> bool`).

**Remarque :** j'attire votre attention sur le code de cette fonction. Passer un moment à comprendre pourquoi elle fait bien ce qu'on lui demande est instructif.
### Docstrings
Pour expliquer le fonctionnement d’une fonction, on lui ajoute un commentaire juste sous la ligne de définition. En Python ces commentaires sont appelés docstrings. On peut y accéder dans le code source ou simplement en utilisant la fonction help().

Ce commentaire doit décrire le rôle de la fonction, ainsi que des conditions éventuelles sur les valeurs des arguments (on parle de **préconditions**).

!!! example "2 exemples"
    === "Sans précondition"
        ``` python
        def est_pair(n : int) -> bool :
            """ renvoie True si n est pair et False sinon"""
            return n % 2 == 0
        ```
        
    === "Avec précondition"
        ``` python
        def nb_boites(n : int) -> int:
            """ renvoie le nombre de boîtes de 6 oeufs nécessaires pour ranger n oeufs
            précondition : n >= 0
            """ 
            # À retrouver dans l'onglet "Exercices" !
        ```
    
## Portée des variables
!!! info "Variable locale et variable globale"
    Lorsque vous définissez des variables à l’intérieur d’une fonction, ces variables ne sont accessibles qu’à la
    fonction elle-même : on parle de **variables locales**. À chaque appel de la fonction, Python va allouer un espace mémoire et un espace de noms pour ces variables.

    Les variables définies à l’extérieur d’une fonction sont des **variables globales**. Leur contenu est visible depuis
    l’intérieur d’une fonction mais cette dernière ne peut pas le modifier. Python considère alors qu’il s’agit d’une
    nouvelle variable (même si visuellement vous lui avez donné le même nom !)


!!! example "Variable locale et variable globale"
    === "2 variables `a` sont créées "
        ``` python
            def f() :
            a = 2
            print(f"Valeur de a dans le corps de la fonction : {a} "
            print('-----------------')
            
            a = 5

            print("Valeur de a dans la partie principale,")
            print(f"avant appel de la fonction f : {a}")
            print('-----------------')

            f()
                
            print("Valeur de a dans la partie principale,")
            print(f"après appel de la fonction f : {a}"
        ``` 
        Sortie :

        ``` output
            Valeur de a dans la partie principale,
            avant appel de la fonction f : 5
            -----------------
            Valeur de a dans le corps de la fonction : 2 
            -----------------
            Valeur de a dans la partie principale,
            après appel de la fonction f : 5
        ```
    === "Une fonction ne peut pas modifier une variable globale"
        ``` python
            def f() :
            a = a - 3
            print(f"Valeur de a dans le corps de la fonction : {a} "
            print('-----------------')
            
            a = 5

            print("Valeur de a dans la partie principale,")
            print(f"avant appel de la fonction f : {a}")
            print('-----------------')

            f()
                
            print("Valeur de a dans la partie principale,")
            print(f"après appel de la fonction f : {a}"
        ``` 
        
        ``` output
            Valeur de a dans la partie principale,
            avant appel de la fonction f : 5
            -----------------
            Valeur de a dans le corps de la fonction :  
            Traceback (most recent call last):

            File "sanstitre0.py", line 23, in <module>
            f()

            File "sanstitre0.py", line 12, in f
            a = a - 3

            UnboundLocalError: local variable 'a' referenced before assignment
        ```
    === "Les variables globales dans une fonction"
        En l'absence de variable locale du même nom, une variable globale est vue et peut être utilisée (mais non modifiée) à l'intérieur d'une fonction :
        ``` python
            def f() :
                print(f"Valeur de a dans le corps de la fonction : {a} ")
                print('-----------------')
    
            a = 5

            print("Valeur de a dans la partie principale,")
            print(f"avant appel de la fonction f : {a}"
            print('-----------------')

            f()
    
            print("Valeur de a dans la partie principale,")
            print(f"après appel de la fonction f : {a}")
        ```
        
        ``` output
            Valeur de a dans la partie principale,
            avant appel de la fonction f : 5
            -----------------
            Valeur de a dans le corps de la fonction : 5
            -----------------
            Valeur de a dans la partie principale,
            après appel de la fonction f : 5
        ```

!!! warning "À connaître mais à éviter "
    Vous pouvez toutefois modifier cette règle que s’est fixée Python. Pour réaliser cela, il vous suffit
    d’utiliser l’instruction `global` suivi du nom de la variable à l’intérieur de la fonction. C'est toutefois fortement déconseillé car elles peuvent introduire des changements indésirables dans d'autres parties du programme.

    En programmation, la `global`isation est généralement une mauvaise chose. 

    **Vous devrez l'éviter dans tous vos programmes cette année**

    ===  "avec `global`"

        ``` python
            def f() :
                global a
                a = a - 3
                print(f"Valeur de a dans le corps de la fonction : {a} "
                print('-----------------')
                
            a = 5

            print("Valeur de a dans la partie principale,")
            print(f"avant appel de la fonction f : {a}")
            print('-----------------')

            f()
                
            print("Valeur de a dans la partie principale,")
            print(f"après appel de la fonction f : {a}"
        ```

        ``` output
            Valeur de a dans la partie principale,
            avant appel de la fonction f : 5
            -----------------
            Valeur de a dans le corps de la fonction : 2 
            -----------------
            Valeur de a dans la partie principale,
            après appel de la fonction f : 2
        ```
    === "Comment faire alors ?"
        Si l'on veut malgré tout que `a` soit modifiée par la fonction `f`, le plus simple est d'utiliser un paramètre et une valeur de retour. Par exemple :

        ``` python
            def f(x) :
                x = x - 3
                return x
                
            a = 5

            print("Valeur de a dans la partie principale,")
            print(f"avant appel de la fonction f : {a}")
            print('-----------------')

            a = f(a)
                
            print("Valeur de a dans la partie principale,")
            print(f"après appel de la fonction f : {a}"
        ``` 
        
        ``` output
            Valeur de a dans la partie principale,
            avant appel de la fonction f : 5
            -----------------
            Valeur de a dans la partie principale,
            après appel de la fonction f : 2
        ```
## Pour compléter
### Le `return` est définitif

!!! warning "Mise en garde"

    Une fonction ne peut exécuter qu’un `return`, tout `return` est définitif.

!!! example "Exemples à utiliser avec précaution"
    === "Après le `return`"
        Tout ce qui suit le `return` n’est pas exécuté, on parle de code mort (dead code en anglais).
        ``` python
        def exemple():
            return "Bonjour"
            print("Ce code ne sera pas exécuté(dead code)")

        # appel
        exemple()
        ```
        ``` output
            'Bonjour'
        ```
    === "Avec plusieurs `return`"
        Il peut y avoir plusieurs `return` mais un seul sera exécuté
        ``` python
        def bonjour(genre):
            if genre == "M":
                return "Bonjour monsieur"
            elif genre == "F":
                return "Bonjour madame"

        # appel
            bonjour("F")

        ```
        ``` output
            'Bonjour madame'
        ```
    === "Le `else` n'est pas nécessaire"
        Il n’est pas nécessaire de placer un `return` dans un `else` car si on arrive après les conditions, ces conditions sont forcément *non vérifiées*.  
        ``` python
        def bonjour(genre):
            if genre == "M":
                return "Bonjour monsieur"
            elif genre == "F":
                return "Bonjour madame"
            return "Bonjour"
        # appel
            bonjour("G")

        ```
        ``` output
            'Bonjour'
        ```

!!! done "Conseil"
    Je vous conseille de n'utiliser qu'un seul `return` par fonction en ajoutant si besoin une variable. La fonction est alors plus facile à lire et à écrire. Notre dernier exemple devient alors :

    ``` python
    def bonjour(genre):
        if genre == "M":
            message = "Bonjour monsieur"
        elif genre == "F":
            message = "Bonjour madame"
        else :
            message = "Bonjour"
        return message
    # appel
        bonjour("G")

    ```
    ``` output
        'Bonjour'
    ```

### Utiliser des fonctions prédéfinies
#### Les fonctions "builtin" de python
De nombreuses fonctions sont utilisables avec python. Voici quelques exemples déjà vus en classe :

!!! example "Déjà vu en classe"
    === "`print` et `input`"
        ``` python
            a = input('entrez votre prénom')
            print('Bonjour', prénom)
        ```
    === "`type`"
        ``` python
            type("Bonjour")
        ```
    === "`int`"
        ``` python
            a = int(input('Entrez votre âge'))
        ```

#### Les fonctions importées d'une bibliothèque
Un très grand nombre de fonctions additionnelles ont été écrites pour des usages variés.

Ces fonctions sont stockées dans des programmes Python appelées **modules** ou **bibliothèques**. Par exemple le module
`math` contient les fonctions mathématiques usuelles et le module `random` contient plusieurs types de
générateurs de nombres pseudo-aléatoires.

Pour importer une fonction d’un module on peut procéder de deux façons :

=== "Première méthode"
    ``` python
        import math
        racine = math.sqrt(2)
    ```
    La fonction `sqrt` (**sq**uare **r**oo**t**) du module `math` renvoie la racine carrée d'un nombre.
=== "Deuxième méthode"
    ``` python
        #import de la fonction sqrt du module math
        from math import sqrt
        racine = sqrt(2)
        #Pour importer toutes les fonctions de math, écrire
        #from math import *
    ```
    La fonction `sqrt` (**sq**uare **r**oo**t**) du module `math` renvoie la racine carrée d'un nombre.

Cette année, nous utiliserons principalement 3 bibliothèques : `math`, `random` et `turtle`.

#### Utiliser sa propre bibliothèque !
Avec Python, on peut écrire et utiliser sa propre bibliothèque de fonctions et c'est tout simple !

 il suffit d'écrire des fonctions dans un programme nommé par exemple `ma_bibliotheque.py`, puis en écrivant un programme placé dans le même répertoire, on peut procéder comme dans le paragraphe précédent avec `from ma_bibliotheque import *`.
